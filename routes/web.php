<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AsistenciaController;
use App\Http\Controllers\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
})->name('dashboard')->middleware('auth');

Route::post('/get-user', [AsistenciaController::class,'getuser'])->name('getuser')->middleware('auth');
Route::get('/reporte', [AsistenciaController::class,'reporte'])->name('reporte')->middleware('auth');

Route::get('login', [LoginController::class,'showLogin'])->name('login');
Route::post('dologin', [LoginController::class,'doLogin'])->name('doLogin');
Route::get('registration', [LoginController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [LoginController::class, 'customRegistration'])->name('register.custom');
Route::get('signout', [LoginController::class, 'signOut'])->name('signout');
