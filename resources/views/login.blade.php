<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="./dist/css/bootstrap.min.css">
  </head>
  <body class="container">
    <h1>ASISTENCIA RA III</h1>

    <!-- if there are login errors, show them here -->

    <form class="" action="{{route('doLogin')}}" method="POST">
      @csrf
    <p>
        <label for="">Usuario</label>
        <input type="text" class="form-control" name="email" value="">
    </p>

    <p>
        <label for="">Contraseña</label>
        <input type="password" class="form-control" name="password" value="">
    </p>
    <button type="submit" class="btn btn-primary" name="button">INGRESAR</button>
    </form>
    <p class="text-muted text-center">Versión 1.0.0</p>
  </body>
</html>
