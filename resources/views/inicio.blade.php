<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="./dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="./custom.css">
  <title></title>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Asistencia INEI</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li></li>
    </ul>
    <span class="navbar-text">
      {{ Auth::user()->name }}
    </span>
    <a class="ml-2 btn btn-primary" href="{{route('signout')}}">SALIR</a>
  </div>
</nav>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-6 mt-4">
        <form class="" id="find">
          <div class="input-group mb-3 input-group-lg">
              <input type="text" class="form-control" placeholder="Pasar Lector de código de Barra" id="dni" name="" value="" required autocomplete="off" autofocus>
              <div class="input-group-append">
                  <button class="btn btn-primary" type="submit" name="button">BUSCAR</button>
              </div>
          </div>
      </form>
      </div>
    </div>
    <div class="row border border-success rounder mt-3" id="contenedor">
      <div class="col-8">
        <label for="" id="message"></label><br>
        <label for="" id="dnii"></label>
        <br>
        <label for="" id="names"></label>
        <br>
        <label for="" id="sede"></label>
        <br>
        <label for="" id="local"></label>
        <br>
        <label for="" id="cargo"></label><br>
        <!--<label for="" id="ranking"><b class="text-info">RANKING NRO: </b>241</label><br>-->
        {{-- <label for="" id="tipo"></label> --}}
      </div>
      <div class="col-4">
        <div class="mt-4 d-flex flex-column justify-content-center border border-dark bg-white">
          <h4 class="text-center">Aula</h4>
          <h1 class="text-center"><b id="aula">1</b></h1>
        </div>
        <div class="mt-1 d-flex flex-column justify-content-center">
          <h6 class="text-center text-uppercase" id="tipo"></h6><!--
          <h6 class="text-center text-uppercase" id="vacunas"></h6>
          <h6 class="text-center text-uppercase" id="observacion"></h6>-->
        </div>
      </div>
    </div>
  </div>

</div>
<script type="text/javascript" src="./dist/jquery.min.js"></script>
<script type="text/javascript" src="./dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./asistencia.js"></script>
</body>
</html>
