<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="./dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="./custom.css">
  <title></title>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Asistencia INEI</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li></li>
    </ul>
    <span class="navbar-text">
      {{ Auth::user()->name }}
    </span>
    <a class="ml-2 btn btn-primary" href="{{route('signout')}}">SALIR</a>
  </div>
</nav>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12">
        <table class="table table-sm " id="example">
          <thead>
            <th>#</th>
            <th>Nombres y apellidos</th>
            <th>dni</th>
            <th>aula</th>
            <th>tipoPostulante</th>
            <th>día</th>
            <th>cargo</th>
            <th>Sede</th>
            <th>1 dosis</th>
            <th>2 dosis</th>
            <th>fch 2 dosis</th>
            <th>ranking</th>
          </thead>
          <tbody>
            <?php foreach ($postulantes as $key => $value): ?>
              <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->name}} {{$value->last_name_p}} {{$value->last_name_m}}</td>
                <td>{{$value->dni}}</td>
                <td>{{$value->cargo->name}}</td>
              </tr>
            <?php endforeach; ?>
          </tbody>
          <tfoot>
            <th>#</th>
            <th>Nombres y apellidos</th>
            <th>dni</th>
            <th>aula</th>
            <th>tipoPostulante</th>
            <th>día</th>
            <th>cargo</th>
            <th>Sede</th>
            <th>1 dosis</th>
            <th>2 dosis</th>
            <th>fch 2 dosis</th>
            <th>ranking</th>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
<script type="text/javascript" src="./dist/jquery.min.js"></script>
<script type="text/javascript" src="./dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./asistencia.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  // Setup - add a text input to each footer cell
  $('#example tfoot th').each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
  } );

  // DataTable
  var table = $('#example').DataTable({
      initComplete: function () {
          // Apply the search
          this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                  if ( that.search() !== this.value ) {
                      that
                          .search( this.value )
                          .draw();
                  }
              } );
          } );
      }
  });

} );
</script>
</body>
</html>
