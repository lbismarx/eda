$("#contenedor").hide();
$("#find").submit(function(e){
  e.preventDefault();
  let dni = $("#dni").val();
  let tipo = "";
  $("#contenedor").removeClass("bg-danger");
  $("#contenedor").removeClass("bg-success");
  $("#contenedor").removeClass("bg-warning");
  $("#contenedor").removeClass("bg-info");
  $("#contenedor").removeClass("inei-bg-danger");
  $("#contenedor").removeClass("border-danger");
    $("#contenedor").removeClass("inei-bg-danger");
    $("#contenedor").removeClass("inei-bg-success");
    $("#contenedor").removeClass("border-danger");
    $("#contenedor").removeClass("border-warning");
  $.ajax({
        type: "POST",
        dataType: "json",
        url:'get-user',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {dni:dni},
        success: function(data) {
          console.log(data);
          if (data.status) {
            if (!data.asistencia) {
              $("#message").html("<b>"+data.message+"</b>");
              $("#contenedor").addClass("inei-bg-success");
            }else {
              $("#message").html("<b>"+data.message+"</b>")
              $("#contenedor").addClass("inei-bg-danger");
              $("#contenedor").addClass("border-danger");
            }
            if(data.tipo_postulante === 'Reserva'){
              $("#contenedor").addClass("inei-bg-reserva");
            }
            $("#dnii").html("<b class='text-info'>DNI: </b>"+data.dni);
            $("#names").html("<h5><b>"+data.name/*+" "+data.last_name_p+" "+data.last_name_m*/+"</b></h5>");
            $("#sede").html("<b class='text-info'>Sede: </b>PUNO - JULIACA</label>");
            $("#local").html("<b class='text-info'>Local: </b>"+data.sede+"</label>");
            $("#cargo").html("<b class='text-info'>Cargo: </b>"+data.cargo+"</label>");
            $("#tipo").html("<b class='text-info'>Tipo: </b>"+data.tipo_postulante+"</label>");
            $("#aula").html(data.aula);
            $("#vacunas").html("<b class='text-info'>VACUNAS (COVID-19): </b><br><b>"+(data.primera_dosis==true?"1 Dosis":"NO")+" | "+(data.segunda_dosis==true?"2 Dosis":"NO")+" | "+(data.fch_seg_dosis?data.fch_seg_dosis:"NO")+"</b>");
            $("#ranking").html("<b class='text-info'>RANKING: </b><b>"+data.ranking+"</b>");
            $("#observacion").html("<b class='text-info'>OBSERVACIÓN: </b><b>"+data.observacion+"</b>");
            $("#contenedor").show();
          }else {
            if (data.asistencia) {
              $("#contenedor").addClass("inei-bg-warning");
              $("#contenedor").addClass("border-warning");
              $("#message").html("<b class='text-dark'>"+data.message+"</b>")
              $("#dnii").html("<b class='text-info'>DNI: </b>"+data.dni);
              $("#names").html("<b>"+data.name/*+" "+data.last_name_p+" "+data.last_name_m*/+"</b>");
              $("#sede").html("<b class='text-info'>Sede: </b>PUNO - JULIACA</label>");
              $("#local").html("<b class='text-info'>Local: </b>"+data.sede+"</label>");
              $("#cargo").html("<b class='text-info'>Cargo: </b>"+data.cargo+"</label>");
              $("#tipo").html("<b class='text-info'>Tipo: </b>"+data.tipo_postulante+"</label>");
              $("#aula").html(data.aula);
              $("#vacunas").html("<b class='text-info'>VACUNAS (COVID-19): </b><br><b>"+(data.primera_dosis==true?"1 Dosis":"NO")+" | "+(data.segunda_dosis==true?"2 Dosis":"NO")+" | "+(data.fch_seg_dosis?data.fch_seg_dosis:"NO")+"</b>");
              $("#ranking").html("<b class='text-info'>RANKING: </b><b>"+data.ranking+"</b>");
              $("#observacion").html("<b class='text-info'>OBSERVACIÓN: </b><b>"+data.observacion+"</b>");
              $("#contenedor").show();
            }else {
              $("#contenedor").addClass("bg-danger");
              $("#message").html("<b>"+data.message+"</b>")
              $("#dnii").html("");
              $("#names").html("");
              $("#sede").html("");
              $("#local").html("");
              $("#cargo").html("");
              $("#tipo").html("");
              $("#aula").html("");
              $("#contenedor").show();

            }
          }

          $("#dni").val("").focus();
          /*tipo = data.tipo_postulante == 'T' ? 'TITULAR':'RESERVA';
          $("#dnii").html("<b class='text-info'>DNI: </b>"+data.dni);
          $("#names").html("<b class='h4'>"+data.name+" "+data.last_name_p+" "+data.last_name_m+"</b>");
          $("#sede").html("<b class='text-info'>Sede: </b>PUNO - JULIACA</label>");
          $("#local").html("<b class='text-info'>Local de Capacitación: </b>"+data.sede+"</label>");
          $("#cargo").html("<b class='text-info'>Cargo: </b><b>"+data.cargo+"</b></label>");
          $("#tipo").html(tipo);
          $("#aula").html(data.aula);
          $("#contenedor").show();
          $("#dni").val("").focus();*/
        }
      });
});
