<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usersede extends Model
{
  use HasFactory;
  protected $table = 'usersede';
  public $timestamps = true;
}
