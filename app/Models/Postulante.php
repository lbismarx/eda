<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cargo;

class Postulante extends Model
{
    use HasFactory;
    protected $table = 'postulante';
    public $timestamps = true;

    public function cargo(){
      return $this->hasOne(Cargo::class,'id','id_cargo');
    }

    public function sede(){
      return $this->hasOne(Sede::class,'id','id_sede');
    }

    public function tipoPostulante(){
      return $this->tipo_postulante == 'T' ? 'Tarde' : ($this->tipo_postulante == 'M' ? 'Mañana': 'No definido');
    }

    public function dosisPostulante(){
      return $this->segunda_dosis == true ? '02 DOSIS' : ('01 DOSIS | '.$this->fch_seg_dosis);
    }
}
