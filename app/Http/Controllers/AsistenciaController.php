<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Postulante;
use App\Models\Cargo;
use App\Models\Asistencia;
use App\Models\Sede;
use App\Models\Usersede;
use Auth;
use View;

class AsistenciaController extends Controller
{
  public function getuser(Request $request){
    $postulante = Postulante::where('dni',$request->dni)->first();
    $status = false;
    if($postulante){
      $today = date('Y-m-d');

      $register =false;

      $dias = '';

      foreach (json_decode($postulante->days) as $key => $value) {
        $dias .= $value.', ';
        if ($today == $value) {
          $register=true;
        }
      }
      $message='';

        if ($register) {
          $existAsistencia = Asistencia::where('dni',$postulante->id)->where('day',$today)->first();
          if (!$existAsistencia) {
            $now = date('H:i:s');

            $asistencia = new Asistencia();
            $asistencia->dni = $postulante->id;
            $asistencia->day = $today;
            $asistencia->time = $now;
            $asistencia->save();

            $asis = false;
            $message = "DNI ENCONTRADO";
          }else {
            $asis = true;
            $message = "DNI YA FUE REGISTRADO";
          }
          $status = true;
        }else {
          $status = false;
          $asis = true;
          $message = "DNI SE REGISTRA los días ".$dias;
        }




      $response = [
        "status" => $status,
        "message" => $message,
        "asistencia" => $asis,
        "dni" => $postulante->dni,
        "name"  => $postulante->name,
        "last_name_p" => $postulante->last_name_p,
        "last_name_m" => $postulante->last_name_m,
        "sede" => $postulante->sede?$postulante->sede->name:"NO DEFINIDO",
        "cargo" => $postulante->cargo?$postulante->cargo->name:"NO DEFINIDO",
        "tipo_postulante" => $postulante->tipoPostulante(),
        "aula" => $postulante->aula,
        "dosis" => $postulante->dosisPostulante(),
        "primera_dosis" => $postulante->primera_dosis,
        "segunda_dosis" => $postulante->segunda_dosis,
        "fch_seg_dosis" => $postulante->fch_seg_dosis,
        "ranking" => $postulante->numero_informe, //Numero ranking
        "observacion" => $postulante->observacion,
      ];
    }else {
      $response =[
        "status" => $status,
        "message" => "DNI NO ENCONTRADO EN EL PADRON"
      ];
      // code...
    }
    return response()->json($response);
  }
  public function reporte(){
    $postulante = Postulante::all();
    $response = [
      "postulantes" => $postulante
    ];
    return View::make('reporte',$response);
  }
}
