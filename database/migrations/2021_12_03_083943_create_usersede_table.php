<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersedeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersede', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_sede');
            $table->unsignedBigInteger('id_users')->unique();
            $table->timestamps();

            $table->foreign('id_sede')->references('id')->on('sede');
            $table->foreign('id_users')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersede');
    }
}
