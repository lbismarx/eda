<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPostulanteTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('postulante', function (Blueprint $table){
      $table->boolean('primera_dosis');
      $table->boolean('segunda_dosis');
      $table->date('fch_seg_dosis')->nullable();
      $table->string('numero_informe',100)->nullable()->default('000');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    //
  }
}
