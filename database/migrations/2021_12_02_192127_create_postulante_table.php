<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostulanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulante', function (Blueprint $table) {
            $table->id();
            $table->string('name',250);
            $table->string('last_name_p',250)->nullable();
            $table->string('last_name_m',250)->nullable();
            $table->string('dni',10)->unique();
            $table->string('cellphone',100)->nullable();
            $table->string('email',100)->nullable();
            $table->string('aula',100);
            $table->enum('tipo_postulante',['R','T'])->comment('R:Reserva,T:Titular') ->default('T');
            $table->string('days',250);
            $table->unsignedBigInteger('id_cargo');
            $table->unsignedBigInteger('id_sede');
            $table->timestamps();

            $table->foreign('id_cargo')->references('id')->on('cargo');
            $table->foreign('id_sede')->references('id')->on('sede');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulante');
    }
}
