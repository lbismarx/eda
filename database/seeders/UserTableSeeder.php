<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use App\Models\User;

class UserTableSeeder extends Seeder
{

  public function run()
  {
    User::create(array(
      'name'     => 'Chris Sevilleja',
      'email'    => 'chris@example.com',
      'password' => Hash::make('awesome'),
    ));
  }

}
